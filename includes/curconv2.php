<?php 

if(!function_exists('curl_cur2'))
{
	function curl_cur2($url)
	{
		$ch = curl_init();
		$timeout = 0;
		 curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt ($ch, CURLOPT_USERAGENT,
					 "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$rawdata = curl_exec($ch);
		curl_close($ch);
		return $rawdata;
	}
}

if(!function_exists('get_currency_floatrates2'))
{
	function get_currency_floatrates2($from_Currency)
	{
		$rawdata  = curl_cur2('http://www.floatrates.com/daily/'.strtolower($from_Currency).'.json');
		$rawdata = json_decode($rawdata, true);
		
		if(isset($rawdata['usd']['rate']) && isset($rawdata['gbp']['rate']))
			return array('eur2usd' => $rawdata['usd']['rate'], 'eur2gbp' => $rawdata['gbp']['rate']);
		else
			return false;
	}
}

if(!function_exists('get_currency_exrateapi2'))
{
	function get_currency_exrateapi2()
	{
		$rawdata  = file_get_contents('https://api.exchangeratesapi.io/'.date('Y-m-d'));
		$rawdata = json_decode($rawdata, true);
		if(isset($rawdata['rates']['USD']) && isset($rawdata['rates']['GBP']))
			return array('eur2usd' => $rawdata['rates']['USD'], 'eur2gbp' => $rawdata['rates']['GBP']);
		else
			return false;
	}
}

if(!function_exists('getRate2'))
{
	function getRate2()
	{	
		$result = array('eur2usd' => 0, 'eur2gbp' => 0);
		
		$result = get_currency_exrateapi2();
		
		if(!$result)
		{
			$result = get_currency_floatrates2('EUR');
			return $result;
		}
		
		return $result;
	}
}

?>