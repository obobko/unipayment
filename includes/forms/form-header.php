<?php
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'http://crm.brfxtrade.com/ajax/psp/getpsp.php');      
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);      
			curl_setopt($curl, CURLOPT_POST, true);      
			curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query(array('leadid' => $_SESSION['leadid']))));      
			$out = curl_exec($curl);      
			curl_close($curl); 
			
if(in_array($_SESSION['country'], array('Hong Kong', 'Sudan')))
	$this->adf_form_default = 'orange';
else
	$this->adf_form_default = $out;

if ($user_info['rememberBilling'] == '0'){
  $remember = false;  
}else{
  $remember = true;
}

if(!$remember){
  $geo = $this->getLocationInfoByIp();
}

$countries = '';
foreach($countries_array as $key => $value){
  if (!$remember && $geo['country'] == $key){
    $countries .= '<option selected value='.(($this->adf_form_default=="nextpay")?$countries_array[$key]['iso3']:$countries_array[$key]['iso']).'>'.$key.'</option>';  
  }
  if ($remember && ( $user_info['country'] == $key || $user_info['country'] == $countries_array[$key]['iso'] )){
    $countries .= '<option selected value='.(($this->adf_form_default=="nextpay")?$countries_array[$key]['iso3']:$countries_array[$key]['iso']).'>'.$key.'</option>';  
  }
  $countries .= '<option value='.(($this->adf_form_default=="nextpay")?$countries_array[$key]['iso3']:$countries_array[$key]['iso']).'>'.$key.'</option>';
}
  
$months = '';
for ( $i = 1; $i <= 12; $i ++ ) {
  if ($i<10){$i = '0'.$i;}
  $months .= '<option value="'.$i.'">'.$i.'</option>';
}

$years = '';
for ( $i = date( "y" ); $i <= date( "y" ) + 15; $i ++ ) {
  $years .= "<option value=20".$i.">20".$i."</option>";
}

$city = '';
if ($remember){
  $city = $user_info['city'];
}

$zip_code = '';
if ($remember){
  $zip_code = $user_info['zip_code'];
}
$address = '';
if ($remember){
  $address = $user_info['address'];
  $phone = $user_info['phone'];
}
$form_balance = '';
if ($user_info['balance'] == ''){
  $form_balance = 0;
}else{
  $form_balance = $user_info['balance'];
}


$billingFname = '';
$billingLname = '';

if(isset($_SESSION['firstname']) && isset($_SESSION['lastname']) && !empty($_SESSION['firstname']) && !empty($_SESSION['lastname']))
{
  $billingFname = $_SESSION['firstname'];
  $billingLname = $_SESSION['lastname'];
}
elseif(isset($_SESSION['nname']))
{
  $tname = explode(' ', $_SESSION['nname']);
  if(isset($tname[0]))
    $billingFname = $tname[0];
  if(isset($tname[1]))
    $billingLname = $tname[1];
}


?>
<!--new popup-->
<!--Terms and condition popup-->
<div class="brfxPopupCont brfxDepErrorPopup">
  <div class="brfxPopup">
    <div class="brfxDepErrorTxtCont brfx--font--roboto-Light">
      <span class="fa_times_box"><i class="fa fa-times fa-5x" aria-hidden="true"></i></span><h2 class="title"><?php pll_e('depwasuns'); ?></h2><p class="modal_text">'+ReplyDesc+'</p>
      <div class="brfx--btn--blue popup_btn_box brfxDepErrorPopupEdit brfxPopupBtn"><a href="#"><?php pll_e('edit deposit'); ?></a></div>
      <p class="brfxDepErrorPopupHelp"><?php pll_e('we-can-assist'); ?></p>
    </div>
  </div>
</div>
<!--new popup-->
<h1 class="brfx--font-h1"><?php pll_e('Deposit'); ?></h1>
<div class="dep-form-head">
  <div class="brfx--form-block-title">
    <div class="brfx--icon--cash brf--float-left brfx-icon-form"></div>
    <h2 class="brfx--font-h2 brfx--font-h2-x"><?php pll_e('Payment method'); ?></h2>
  </div>
  <div class="pf_row">
    <div class="cc_bw_btns_container clear">
    <span id="adf_cc_btn" class="adf_head_icons adf_icon_selected">
      <i class="brfx--icon--cart"></i>
      <div class="adf_head_iconsTxt"><?php pll_e('Credit Card'); ?></div>
    </span>
    <span id="adf_bw_btn" class="adf_head_icons">
      <i class="brfx--icon--house"></i>
      <div class="adf_head_iconsTxt"><?php pll_e('Bank Wire'); ?></div>
    </span>
    </div>
  </div>
</div>
<div class="brfxCreditTypesBox">
  <p><?php pll_e('suppaymet'); ?></p><span><img src="<?php echo plugins_url('unipayForm/assets/img/creditCard/Visa.jpg') ?>" alt=""></span>
                            <span><img src="<?php echo plugins_url('unipayForm/assets/img/creditCard/Maestro.jpg') ?>" alt="">
                            </span><span><img src="<?php echo plugins_url('unipayForm/assets/img/creditCard/Master.jpg') ?>" alt=""></span>
</div>
