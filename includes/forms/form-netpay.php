<!--NETPAY FORM-->
<?php 
include( plugin_dir_path( __FILE__ ) . 'form-header.php');    
?>
<div class="adfFormsWrap">
  <div id="adf_redir_cc" style="display: none;">You will be redirected to the payment form page</div> <!--orange redirect to the cc form-->
  <form method="POST" class="unipayForm" data-form="nextpay" action="javascript:void(null);">
    <input type="hidden" class="adf_selected_form" value="<?php echo $this->adf_form_default; ?>" /><!--selected form-->
    <input type="hidden" id="u_email" value="<?php echo $_SESSION['email']; ?>">
    <input type="hidden" class="adf_url" value="<?php echo admin_url( 'admin-ajax.php' ) ?>">
   
    <div>
        <fieldset class="personalInfo">
      <div class="brfx--form-block-title">
        <div class="brfx--icon--moneyup brf--float-left brfx-icon-form"></div>
        <h2 class="brfx--font-h2 brfx--font-h2-x"><?php pll_e('Funds'); ?></h2>
      </div>
      <?php
        $firstdep = '';
        if ($_SESSION['firstDepositDate'] == ''){
           $firstdep = '<p class="fundsP">'.pll__('thecurwillfix').'</p>';
        }
        _e($firstdep,  'unipayForm');
      ?>
      <div class="brfx--form-block brfxDepFloatBoxWrap brfxOverflow">
        <div class="box1 brfxDepFloatBox">
          <div class="lab brfx--small-grey">
            <label for="Amount"></label>
          </div>
          <div class="field">
            <input value="" type="number" class="depFieldFocus brfx--input" id="Amount" name="Amount" data-form="Amount" placeholder="<?php pll_e('Enter amount'); ?>" maxlength="10">
            <div id="Amount_error" class="error-box"><?php pll_e('pl-en-valamount'); ?></div>
          </div>
        </div>
        <div class="pf_row box_sd brfxDepFloatBox">
          <div class="lab brfx--small-grey">
            <label for="Currency"></label> 
          </div>
          <div class="field">
            <?php
              $currency = '';
              if ($_SESSION['firstDepositDate'] != ''){
                 $currency = 'disabled';
              }
              $c_ch1 = '';
              $c_ch2 = '';
              
              if( $_SESSION['firstDepositDate'] == ''){
                $c_ch2 = 'selected';
              }else{
                if ($user_info['currency'] == 'USD'){
                  $c_ch1 = 'selected';
                }else{
                  $c_ch2 = 'selected';
                } 
              }
              
              $currency_options = '<option value="1" '.$c_ch1.'>USD</option><option value="2" '.$c_ch2.'>EUR</option>';
            ?>
            <select class="brf--select" name="Currency" id="Currency" <?php echo $currency; ?>>
              <?php echo $currency_options; ?>
            </select>
          </div>
        </div>
        <div class="balancebox brfxDepFloatBox">
        <p><?php pll_e('Current balance'); ?>: <span class="current_balance"><?php echo $form_balance ?></span> <span class="current_balance_currency"><?php 
    
        if($_SESSION['firstDepositDate'] == ''){
          echo 'EUR'; 
        }else{
          echo $user_info['currency'];
        }
          
          ?></span></p>
        <p><?php pll_e('balafterdep'); ?>: <span class="balance_after_deposit"><?php echo $form_balance ?></span> <span class="balance_after_currency"><?php 
        if($_SESSION['firstDepositDate'] == ''){
          echo 'EUR'; 
        }else{
          echo $user_info['currency'];
        }
          ?></span></p>
        </div>
      </div>
     </fieldset>
    <fieldset class="creditCard">
        <div class="brfx--form-block-title">
          <div class="brfx--icon--cards brf--float-left brfx-icon-form"></div>
          <h2 class="brfx--font-h2 brfx--font-h2-x"><?php pll_e('crcainfo'); ?></h2>
        </div>
        <div class="brfx--form-block">
          <div class="lab brfx--small-grey">
            <label for="Member"><?php pll_e('Cardholder Name1'); ?></label>
              <!--deposit info box-->
              <div class="dep_inf_bx">
                <div class="dep_inf_bx_wrap">
                  <span class="dep_inf_bx_btn">?</span>
                  <div class="dep_inf_bx_win cardHolderNameWindow">
                    <!--info box content-->
                    <img src="<?php echo plugins_url('unipayForm/assets/img/cardholder_name_info.jpg') ?>" alt="" />
                  </div>
                </div>
              </div>
              <!--deposit info box--> 
          </div>
          <div class="field">
            <input type="text" class="depFieldFocus brfx--input" id="Member" name="Member" data-form="Member" placeholder="" maxlength="50">
            <div id="Member_error" class="error-box"><?php pll_e('plexnamascar'); ?></div>
          </div>
        </div>
        <div class="brfx--form-block">
          <div class="lab brfx--small-grey">
            <label for="CardNum"><?php pll_e('Card Number'); ?></label>
          </div>
          <div class="field brfxCardNumPos">
            <input type="text" class="depFieldFocus brfx--input" id="CardNum" name="CardNum" data-form="CardNum" placeholder="0000 0000 0000 0000" maxlength="40">
            <div id="CardNum_error" class="error-box"><?php pll_e('plaencarnumval'); ?></div>
            <div class="credit_card_icon"></div>
          </div>
        </div>
        <div class="brfxDepFloatBoxWrap pf_row brfx--form-block brfxDepTable">
          <div class="brfxDepFloatBox">
              <div class="lab brfx--small-grey">
                <label><?php pll_e('Expiry Date'); ?></label>
              </div>
              <select id="ExpMonth"  class="brf--select" name="ExpMonth"><?php echo $months; ?></select>
          </div>
          <div class="brfxDepFloatBox">
              <select id="ExpYear" class="brf--select" name="ExpYear"><?php echo $years; ?></select>
          </div>  
          <div class="brfxDepFloatBox">
            <div class="lab brfx--small-grey">
              <label for="CVV2"><?php pll_e('CVV'); ?></label>
                <!--deposit info box-->
                <div class="dep_inf_bx">
                  <div class="dep_inf_bx_wrap">
                    <span class="dep_inf_bx_btn">?</span>
                    <div class="dep_inf_bx_win">
                      <!--info box content-->
                      <img src="<?php echo plugins_url('unipayForm/assets/img/cvv_info.jpg') ?>" alt="" />
                    </div>
                  </div>
                </div>
               <!--deposit info box--> 
            </div>
            <div class="field">
              <input type="number" class="depFieldFocus brfx--input" id="CVV2" name="CVV2" data-form="CVV2" placeholder="000" maxlength="5">
              <div id="CVV2_error" class="error-box"><?php pll_e('valcvven'); ?></div>
            </div>
          </div>
        </div>

        <p class="aboutStoreInfo"><?php pll_e('ewsontstore'); ?></p>

      </fieldset>
      <fieldset class="billingAdress">
        <div class="brfx--form-block-title">
            <div class="brfx--icon--cashnavigation brf--float-left brfx-icon-form"></div>
            <h2 class="brfx--font-h2 brfx--font-h2-x"><?php pll_e('bilddare'); ?></h2>
        </div>
        <div class="pf_row address-box brfx--form-block">
          <div>
            <div class="lab brfx--small-grey">
              <label for="BillingAddress1"><?php pll_e('Address'); ?></label>
            </div>
            <div class="field">
              <input type="text" class="depFieldFocus brfx--input" id="BillingAddress1" name="BillingAddress1" data-form="BillingAddress1" placeholder="" maxlength="100" value="<?php echo $address; ?>">
              <div id="BillingAddress1_error" class="error-box"><?php pll_e('plenyouadddre'); ?></div>
            </div>
          </div>
        </div>

        <div class="pf_row address-box brfx--form-block">
          <div>
            <div class="lab brfx--small-grey">
              <label for="BillingCountry"><?php pll_e('Country'); ?></label>
            </div>
            <select id="BillingCountry" class="brf--select" name="BillingCountry"><?php echo $countries ?></select>
          </div>
        </div>  

        <div class="pf_row address-box brfx--form-block">
          <div class="city_zip_box">
            <div class="box1">
              <div class="lab brfx--small-grey">
                <label for="BillingCity"><?php pll_e('City'); ?></label>
              </div>
              <div class="field">
                <input type="text" class="depFieldFocus brfx--input" id="BillingCity" name="BillingCity" data-form="BillingCity" placeholder="" maxlength="60" value="<?php echo $city; ?>">
                <div id="BillingCity_error" class="error-box"><?php pll_e('plencity'); ?></div>
              </div>
            </div>
            <div class="box_sd">
              <div class="lab brfx--small-grey">
                <label for="BillingZipCode"><?php pll_e('Postal/Zip Code'); ?></label>
              </div>
              <div class="field">
                <input type="number" class="depFieldFocus brfx--input" id="BillingZipCode" name="BillingZipCode" data-form="BillingZipCode" placeholder="" maxlength="15" value="<?php echo $zip_code ?>">
                <div id="BillingZipCode_error" class="error-box"><?php pll_e('plenzip'); ?></div>
              </div>
            </div>
          </div>
        </div>
        <?php
          $saved1 = '';
          $saved2 = '';
          if ($remember){
            $saved1 = 'checked';
            $saved2 = 'check';
          }
        ?>
        <div class="brfxDepSaveAddressChkBox brfx--form-block chcustom <?php echo $saved2 ?>">
        <!--checkbox-->
        <div class="brfxCheckbox">      
          <input type="checkbox" id="chcustom" name="chcustom" <?php echo $saved1 ?>>
          <label for="chcustom"><span></span></label>
        </div>
          <p><?php pll_e('svbiladdre'); ?></p>
        </div>
      </fieldset>
    </div>
      
   
    
    <div class="pf_row btn" id="contButtonWrap" style="margin-bottom:20px;">
      <div class="lab brfx--small-grey"></div>
      <div class="brfxFirstSubmitCont field brfx--btn--blue">
        <div class="brfxPreloaderCirclesPopup brfxDepPreloader adf_DepositAn" style="left: -15px;">
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCircleBlue" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div></div>
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCirleGreen" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div></div>
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCircleRed" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div></div>
        </div>
        <span id="orangeRedTxt" style="display: none; position: absolute; margin-top: 40px;">Please, wait a few seconds. You will be redirected to the payment form page.</span>
        <input class="pf_pm_btn sb-btn" type="submit" value="<?php pll_e('Continue'); ?>" />
        <div class="sb-submit_error"><?php pll_e('plchinfi'); ?></div>
      </div>
    </div>

    <input type="hidden" id="termsConditions" name="termsConditions" value="1" />

  </form>
</div>
<?php 
include( plugin_dir_path( __FILE__ ) . 'form-footer.php');    
?>
