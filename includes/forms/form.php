<?php
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, CRM_URL.'ajax/psp/getpsp.php');      
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);      
			curl_setopt($curl, CURLOPT_POST, true);      
			curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query(array('leadid' => $_SESSION['leadid']))));      
			$out = curl_exec($curl);      
			curl_close($curl); 
			
$this->adf_form_default = 'netpay';

if ($user_info['rememberBilling'] == '0'){
  $remember = false;  
}else{
  $remember = true;
}

if(isset($_SESSION['city']))
	$city = $_SESSION['city'];
else
	$city = '';

if(isset($_SESSION['lane']))
	$lane = $_SESSION['lane'];
else
	$lane = '';

if(isset($_SESSION['code']))
	$code = $_SESSION['code'];
else
	$code = '';

if(!$remember){
  $geo = $this->getLocationInfoByIp();
}

$countries = '';
foreach($countries_array as $key => $value){
  if (!$remember && $geo['country'] == $key){
    $countries .= '<option selected value='.(($this->adf_form_default=="nextpay")?$countries_array[$key]['iso3']:$countries_array[$key]['iso']).'>'.$key.'</option>';  
  }
  if ($remember && ( $user_info['country'] == $key || $user_info['country'] == $countries_array[$key]['iso'] )){
    $countries .= '<option selected value='.(($this->adf_form_default=="nextpay")?$countries_array[$key]['iso3']:$countries_array[$key]['iso']).'>'.$key.'</option>';  
  }
  $countries .= '<option value='.(($this->adf_form_default=="nextpay")?$countries_array[$key]['iso3']:$countries_array[$key]['iso']).'>'.$key.'</option>';
}
  
$months = '';
for ( $i = 1; $i <= 12; $i ++ ) {
  if ($i<10){$i = '0'.$i;}
  $months .= '<option value="'.$i.'">'.$i.'</option>';
}

$years = '';
for ( $i = date( "y" ); $i <= date( "y" ) + 15; $i ++ ) {
  $years .= "<option value=20".$i.">20".$i."</option>";
}

if ($remember){
  $phone = $user_info['phone'];
}
$form_balance = '';
if ($user_info['balance'] == ''){
  $form_balance = 0;
}else{
  $form_balance = $user_info['balance'];
}

?>
<!-- Deposit error popup -->
<div class="popup depDepErrorPopup" id="popup_dep__error">
  <div class="popup-window popup-small">
    <div class="depPopup">
      <div class="depDepErrorTxtCont dep--font--roboto-Light">
        <span class="fa_times_box">
        <img src="<?php echo plugins_url('unipayForm/assets/img/error-dep-icon.png') ?>" alt="" />
        </span><h2 class="title"><?php pll_e('depwasuns'); ?></h2><p class="modal_text">'+ReplyDesc+'</p>
        <div class="dep--btn--blue popup_btn_box depDepErrorPopupEdit depPopupBtn" style="overflow: hidden; display: flex; justify-content: center;">
          <span class="button-border cond_btn" id="edit-dep-btn" style="margin-right: 0;">
            <a href="#"  class="edit_dep__2p button button-xs sb-btn"><span><?php pll_e('edit deposit'); ?></span></a>
          </span>
        </div>
        <p class="depDepErrorPopupHelp" style="font-family: 'Open Sans', sans-serif;"><?php pll_e('we-can-assist'); ?></p>
      </div>
    </div>    
  </div>
</div>

<!-- Deposit success popup -->
<div class="popup depDepSuccPopup" id="popup_dep__succss">
  <div class="popup-window popup-xs">
    <div class="succ-dep-popup-cont">
      <div class="depPopupCont">
        <div class="depPopup">
          <div class="depDepErrorTxtCont dep--font--roboto-Light">
          <div class="deposit_page d_p3">
          <div class="d_success">
            <div class="d_sm">
              <img src="<?php echo plugins_url('unipayForm/assets/img/succ-dep-icon.png') ?>" alt="" />
              <h2 class="balanceDepSucc">The deposit was successful</h2>
              <h2 class="balanceDepSucc balanceDepSuccBalance"><span class="d_new_balance"></span></h2>
            </div>
            <div class="d_sm2">
              <p>Confirmation mail was send to your email address</p>
              <p>Transaction ID: <span class="d_transaction_id"></span></p>
            </div>
          </div>
          <div class="d_links">
            <div class="d_center_container">
              <h2>start trading now</h2>
            </div>
            <div class="d_linksWrap depOverflow">
              <span class="button-border">
                  <a href="/web_trader/" class="button button-xs "><span>open web trader</span></a>
              </span>
            </div>
          </div>
        </div>
          </div>
        </div>
      </div>
    </div>      
  </div>
</div>

<!-- PAYMENT METHODS -->
<h1 class="dep--font-h1"><?php pll_e('Deposit'); ?></h1>
<div class="dep-form-head">
  <div class="dep--form-block-title">
    <div class="dep--icon--cash brf--float-left dep-icon-form"></div>
    <h2 class="dep--font-h2 dep--font-h2-x"><?php pll_e('Payment method'); ?></h2>
  </div>
  <div class="pf_row">
    <div class="cc_bw_btns_container clear">
      <div class="pm-container">
        
        <!--MAIN DEPOSIT BUTTON-->
        <div class="pm-dw-box">
          <div class="pm-wrap pm-btn-active" data-type="standart" data-form="bridget1">
            <div class="pm-btn">
              <img src="<?php echo plugins_url('unipayForm/assets/img/logo_icons/visa_icon.png') ?>" alt="" />
              <img src="<?php echo plugins_url('unipayForm/assets/img/logo_icons/mc_icon.png') ?>" alt="" />
              <img src="<?php echo plugins_url('unipayForm/assets/img/logo_icons/maestro_icon.png') ?>" alt="" />
            </div>
          </div>
        </div>

        <!--OTHER OPTIONS BUTTON-->
        <div class="pm-dw-box">
          <div class="pm-wrap" data-type="standart_other" data-form="form-quick">
            <div class="pm-btn" style="justify-content: center;">
              Other options
            </div>
          </div>
        </div>
        
        <!--BANK WIRE BUTTON-->
        <div class="pm-subgroup">
          <div class="pm-wrap" data-type="bw" data-form="bankwire">
            <div class="pm-btn">
              <img src="<?php echo plugins_url('unipayForm/assets/img/logo_icons/bankwire_icon.png') ?>" alt="" />
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="adfFormsWrap">

  <!--
    BRIDGER FORM
   -->
  <form id="bridget1" method="POST" class="unipayForm" data-form="nextpay" action="javascript:void(null);" >
      <input type="hidden" class="adf_selected_form" value="bridget1" />
      <!--selected form-->
      <input type="hidden" id="u_email" value="<?php echo $_SESSION['email']; ?>">
      <input type="hidden" class="adf_url" value="<?php echo admin_url( 'admin-ajax.php' ) ?>">
      <div>
          <fieldset class="personalInfo">
              <div class="dep--form-block-title">
                  <div class="dep--icon--moneyup brf--float-left dep-icon-form"></div>
                  <h2 class="dep--font-h2 dep--font-h2-x">
                      <?php pll_e('Funds'); ?>
                  </h2>
              </div>
          <!--    <?php
         $firstdep = '';
          /*if ($_SESSION['firstDepositDate'] == ''){
             $firstdep = '<p class="fundsP">'.pll__('thecurwillfix').'</p>';
          }*/
          _e($firstdep,  'unipayForm');
        ?> -->
              <div class="dep--form-block depDepFloatBoxWrap depOverflow">
                  <div class="box1 depDepFloatBox">
                      <div class="lab dep--small-grey field__label">
                          <label for="Amount"></label>
                      </div>
                      <div class="field">
                          <input value="" type="number" class="depFieldFocus dep--input" id="Amount-quick" name="Amount" data-form="Amount" placeholder="<?php pll_e('Enter amount'); ?>" maxlength="10">
                          <div id="Amount_error" class="error-box">
                              <?php pll_e('pl-en-valamount'); ?>
                          </div>
                      </div>
                  </div>
                  <div class="pf_row box_sd depDepFloatBox">
                      <div class="lab dep--small-grey">
                          <label for="Currency"></label>
                      </div>
                      <div class="field">
                          <?php
                $currency = '';
                if ($_SESSION['firstDepositDate'] != ''){
                   $currency = 'disabled';
                }
                $c_ch1 = '';
                $c_ch2 = '';

                if( $_SESSION['firstDepositDate'] == ''){
                  $c_ch2 = 'selected';
                }else{
                  if ($user_info['currency'] == 'USD'){
                    $c_ch1 = 'selected';
                  }else{
                    $c_ch2 = 'selected';
                  } 
                }

                $currency_options = '<option value="1" '.$c_ch1.'>USD</option><option value="2" '.$c_ch2.'>EUR</option>';
              ?>
                          <select class="brf--select currency-quick" name="Currency" id="Currency" <?php echo $currency; ?>>
                              <?php echo $currency_options; ?>
                          </select>
                      </div>
                  </div>
                  <div class="balancebox depDepFloatBox">
                      <p>
                          <?php pll_e('Current balance'); ?>: <span class="current_balance">
                              <?php echo $form_balance ?></span> <span class="current_balance_currency">
                              <?php 

          if($_SESSION['firstDepositDate'] == ''){
            echo 'EUR'; 
          }else{
            echo $user_info['currency'];
          }

            ?></span></p>
                      <p>
                          <?php pll_e('balafterdep'); ?>: <span class="balance_after_deposit">
                              <?php echo $form_balance ?></span> <span class="balance_after_currency">
                              <?php 
          if($_SESSION['firstDepositDate'] == ''){
            echo 'EUR'; 
          }else{
            echo $user_info['currency'];
          }
            ?></span></p>
                  </div>
				   <div class="clearfix"></div>
        <div class="amount-notice">

                <?php
        $firstdep = '';
        if ($_SESSION['firstDepositDate'] == ''){
          $firstdep = '<p class="fundsP"><img style="margin-right:10px" class="amount-image-hint" src="'.plugins_url("unipayForm/assets/img/point.png").'" alt="" />'.pll__('thecurwillfix').'</p>';
        }
        _e($firstdep,  'unipayForm');
      ?>
      <p class="fundsP teswt"><img  style="margin-right:10px" class="amount-image-hint" src="<?php echo plugins_url('unipayForm/assets/img/point.png') ?>" alt="" /><?php pll_e('mindeposit'); ?></p>
        </div>
              </div>
          </fieldset>
          <fieldset class="billingAdress">
              <div class="dep--form-block-title">
                  <div class="dep--icon--cashnavigation brf--float-left dep-icon-form"></div>
                  <h2 class="dep--font-h2 dep--font-h2-x">
                      <?php pll_e('bilddare'); ?>
                  </h2>
              </div>
              <div class="pf_row address-box dep--form-block">
                  <div>
                      <div class="lab dep--small-grey field__label">
                          <label for="BillingAddress1">
                              <?php pll_e('Address'); ?></label>
                      </div>
                      <div class="field">
                          <input type="text" class="depFieldFocus dep--input" id="BillingAddress1" name="BillingAddress1" data-form="BillingAddress1" placeholder="<?php pll_e('Address'); ?>" maxlength="100" value="<?php echo $lane; ?>">
                          <div id="BillingAddress1_error" class="error-box">
                              <?php pll_e('plenyouadddre'); ?>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="pf_row address-box dep--form-block">
                  <div>
                      <div class="lab dep--small-grey">
                          <label for="BillingCountry">
                              <?php pll_e('Country'); ?></label>
                      </div>
                      <select id="BillingCountry" class="brf--select" name="BillingCountry">
                          <?php echo $countries ?></select>
                  </div>
              </div>
              <div class="pf_row address-box dep--form-block">
                  <div class="city_zip_box">
                      <div class="box1">
                          <div class="lab dep--small-grey field__label">
                              <label for="BillingCity">
                                  <?php pll_e('City'); ?></label>
                          </div>
                          <div class="field">
                              <input type="text" class="depFieldFocus dep--input" id="BillingCity" name="BillingCity" placeholder="<?php pll_e('City'); ?>" maxlength="60" value="<?php echo $city; ?>">
                              <div id="BillingCity_error" class="error-box">
                                  <?php pll_e('plencity'); ?>
                              </div>
                          </div>
                      </div>
                      <div class="box_sd">
                          <div class="lab dep--small-grey field__label">
                              <label for="BillingZipCode">
                                  <?php pll_e('Postal/Zip Code'); ?></label>
                          </div>
                          <div class="field">
                              <input type="text" class="depFieldFocus dep--input" id="BillingZipCode" name="BillingZipCode" data-form="BillingZipCode" placeholder="<?php pll_e('Postal/Zip Code'); ?>" maxlength="15" value="<?php echo $code; ?>">
                              <div id="BillingZipCode_error" class="error-box">
                                  <?php pll_e('plenzip'); ?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <?php
            $saved1 = '';
            $saved2 = '';
            if ($_SESSION['rememberBilling']){
              $saved1 = 'checked';
              $saved2 = 'check';
            }
          ?>
              <!-- Save Billing Address -->
              <label class="checkbox-field save-billing">
                  <?php pll_e('svbiladdre'); ?>
                  <input id="bill__checkbox" name="chcustom" type="checkbox" <?php echo $saved1 ?>>
                  <span class="checkmark"></span>
              </label>
          </fieldset>
      </div>
      <div class="depDepTermsChkBox dep--form-block chcustom_terms">
        <!--checkbox-->
        <div class="depCheckbox">
          <input type="checkbox" id="ReviewtermsConditionsAgree" name="termsConditionsAgree" value="1">
        </div>
        <!-- Terms second page -->
        <label class="checkbox-field save-billing">
          By clicking 'Deposit' you are indicating that you have read and agreed to <a href="/regulatory-documents/terms-and-conditions" target="_blank">Terms and Conditions</a> and <a href="/regulatory-documents/privacy-policy" target="_blank">Privacy Policy</a>
          <input name="chcustom" type="checkbox" id="terms" data-form="terms">
          <span class="checkmark"></span>
        </label>
        <div class="depDepRevTermErr">
          Please agree to Terms and Conditions            
        </div>
      </div>
      <div class="pf_row btn" id="contButtonWrap" style="margin-bottom:20px;">
          <div class="lab dep--small-grey"></div>
          <div class="depFirstSubmitCont field dep--btn--blue">
              <div class="depPreloaderCirclesPopup depDepPreloader adf_DepositAn" style="left: -15px;">
                  <div class="depPreloaderCrlCont">
                      <div class="depPreloaderCircle depCircleBlue" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div>
                  </div>
                  <div class="depPreloaderCrlCont">
                      <div class="depPreloaderCircle depCirleGreen" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div>
                  </div>
                  <div class="depPreloaderCrlCont">
                      <div class="depPreloaderCircle depCircleRed" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div>
                  </div>
              </div>

              <!-- Submit -->
              <span class="button-border">
                  <a id="bridget1_deposit__submit" href="#" class="button button-xs sb-btn"><span>
                          <?php pll_e('Continue'); ?></span></a>
              </span>
              <div class="sb-submit_error">
                  <?php pll_e('plchinfi'); ?>
              </div>

          </div>
      </div>
      <span style="display:block; max-width: 470px; ">After pressing 'Continue' button you will be redirected to the payment form page.</span>
      <input type="hidden" id="termsConditions" name="termsConditions" value="1" />
  </form>
  <!--
    END OF BRIDGER FORM
   -->  
  
  <!--
    NETPAY FORM
   -->
  <form id="form-quick" method="POST" class="unipayForm" data-form="nextpay" action="javascript:void(null);" style="display:none;">
      <input type="hidden" class="adf_selected_form" value="netpay" />
      <!--selected form-->
      <input type="hidden" id="u_email" value="<?php echo $_SESSION['email']; ?>">
      <input type="hidden" class="adf_url" value="<?php echo admin_url( 'admin-ajax.php' ) ?>">
      <div>
          <fieldset class="personalInfo">
              <div class="dep--form-block-title">
                  <div class="dep--icon--moneyup brf--float-left dep-icon-form"></div>
                  <h2 class="dep--font-h2 dep--font-h2-x">
                      <?php pll_e('Funds'); ?>
                  </h2>
              </div>
              <?php
          $firstdep = '';
          /*if ($_SESSION['firstDepositDate'] == ''){
             $firstdep = '<p class="fundsP">'.pll__('thecurwillfix').'</p>';
          }*/
          _e($firstdep,  'unipayForm');
        ?>
              <div class="dep--form-block depDepFloatBoxWrap depOverflow">
                  <div class="box1 depDepFloatBox">
                      <div class="lab dep--small-grey field__label">
                          <label for="Amount"></label>
                      </div>
                      <div class="field">
                          <input value="" type="number" class="depFieldFocus dep--input" id="Amount-quick" name="Amount" data-form="Amount" placeholder="<?php pll_e('Enter amount'); ?>" maxlength="10">
                          <div id="Amount_error" class="error-box">
                              <?php pll_e('pl-en-valamount'); ?>
                          </div>
                      </div>
                  </div>
                  <div class="pf_row box_sd depDepFloatBox">
                      <div class="lab dep--small-grey">
                          <label for="Currency"></label>
                      </div>
                      <div class="field">
                          <?php
                $currency = '';
                if ($_SESSION['firstDepositDate'] != ''){
                   $currency = 'disabled';
                }
                $c_ch1 = '';
                $c_ch2 = '';

                if( $_SESSION['firstDepositDate'] == ''){
                  $c_ch2 = 'selected';
                }else{
                  if ($user_info['currency'] == 'USD'){
                    $c_ch1 = 'selected';
                  }else{
                    $c_ch2 = 'selected';
                  } 
                }

                $currency_options = '<option value="1" '.$c_ch1.'>USD</option><option value="2" '.$c_ch2.'>EUR</option>';
              ?>
                          <select class="brf--select currency-quick" name="Currency" id="Currency" <?php echo $currency; ?>>
                              <?php echo $currency_options; ?>
                          </select>
                      </div>
                  </div>
                  <div class="balancebox depDepFloatBox">
                      <p>
                          <?php pll_e('Current balance'); ?>: <span class="current_balance">
                              <?php echo $form_balance ?></span> <span class="current_balance_currency">
                              <?php 

          if($_SESSION['firstDepositDate'] == ''){
            echo 'EUR'; 
          }else{
            echo $user_info['currency'];
          }

            ?></span></p>
                      <p>
                          <?php pll_e('balafterdep'); ?>: <span class="balance_after_deposit">
                              <?php echo $form_balance ?></span> <span class="balance_after_currency">
                              <?php 
          if($_SESSION['firstDepositDate'] == ''){
            echo 'EUR'; 
          }else{
            echo $user_info['currency'];
          }
            ?></span></p>
                  </div>
              </div>
          </fieldset>
		   <div class="amount-notice">

                <?php
        $firstdep = '';
        if ($_SESSION['firstDepositDate'] == ''){
          $firstdep = '<p class="fundsP"><img style="margin-right:10px" class="amount-image-hint" src="'.plugins_url("unipayForm/assets/img/point.png").'" alt="" />'.pll__('thecurwillfix').'</p>';
        }
        _e($firstdep,  'unipayForm');
      ?>
      <p class="fundsP teswt"><img  style="margin-right:10px" class="amount-image-hint" src="<?php echo plugins_url('unipayForm/assets/img/point.png') ?>" alt="" /><?php pll_e('mindeposit'); ?></p>
        </div>
          <fieldset class="creditCard">
              <div class="dep--form-block-title">
                  <div class="dep--icon--cards brf--float-left dep-icon-form"></div>
                  <h2 class="dep--font-h2 dep--font-h2-x">
                      <?php pll_e('crcainfo'); ?>
                  </h2>
              </div>
              <div class="dep--form-block">
                  <div class="lab dep--small-grey field__label">
                      <label for="Member">
                          <?php pll_e('Cardholder Name'); ?></label>
                      <!--deposit info box-->
                      <div class="dep_inf_bx">
                          <div class="dep_inf_bx_wrap">
                              <span class="dep_inf_bx_btn">?</span>
                              <div class="dep_inf_bx_win cardHolderNameWindow">
                                  <!--info box content-->
                                  <img src="<?php echo plugins_url('unipayForm/assets/img/cardholder_name_info.jpg') ?>" alt="" />
                              </div>
                          </div>
                      </div>
                      <!--deposit info box-->
                  </div>
                  <div class="field">
                      <input type="text" class="depFieldFocus dep--input" id="Member" name="Member" placeholder="<?php pll_e('Cardholder Name'); ?>" maxlength="50">
                      <div id="Member_error" class="error-box">
                          <?php pll_e('plexnamascar'); ?>
                      </div>
                  </div>
              </div>
              <div class="dep--form-block">
                  <div class="lab dep--small-grey field__label">
                      <label for="CardNum">
                          <?php pll_e('Card Number'); ?></label>
                  </div>
                  <div class="field depCardNumPos">
                      <input type="text" class="depFieldFocus dep--input" id="CardNum" name="CardNum" data-form="CardNum" placeholder="<?php pll_e('Card Number'); ?>" maxlength="40">
                      <div id="CardNum_error" class="error-box">
                          <?php pll_e('plaencarnumval'); ?>
                      </div>
                      <div class="credit_card_icon"></div>
                  </div>
              </div>
              <div class="depDepFloatBoxWrap pf_row dep--form-block depDepTable">
                  <div class="depDepFloatBox">
                      <div class="lab dep--small-grey">
                          <label>
                              <?php pll_e('Expiry Date'); ?></label>
                      </div>
                      <select id="ExpMonth" class="brf--select" name="ExpMonth">
                          <?php echo $months; ?></select>
                  </div>
                  <div class="depDepFloatBox">
                      <select id="ExpYear" class="brf--select" name="ExpYear">
                          <?php echo $years; ?></select>
                  </div>
                  <div class="depDepFloatBox">
                      <div class="lab dep--small-grey field__label">
                          <label for="CVV2">
                              <?php pll_e('CVV'); ?></label>
                          <!--deposit info box-->
                          <div class="dep_inf_bx">
                              <div class="dep_inf_bx_wrap">
                                  <span class="dep_inf_bx_btn">?</span>
                                  <div class="dep_inf_bx_win">
                                      <!--info box content-->
                                      <img src="<?php echo plugins_url('unipayForm/assets/img/cvv_info.jpg') ?>" alt="" />
                                  </div>
                              </div>
                          </div>
                          <!--deposit info box-->
                      </div>
                      <div class="field">
                          <input type="number" class="depFieldFocus dep--input" id="CVV2" name="CVV2" data-form="CVV2" placeholder="000" maxlength="5">
                          <div id="CVV2_error" class="error-box">
                              <?php pll_e('valcvven'); ?>
                          </div>
                      </div>
                  </div>
              </div>
              <p class="aboutStoreInfo">
                  <?php pll_e('ewsontstore'); ?>
              </p>
          </fieldset>
          <fieldset class="billingAdress">
              <div class="dep--form-block-title">
                  <div class="dep--icon--cashnavigation brf--float-left dep-icon-form"></div>
                  <h2 class="dep--font-h2 dep--font-h2-x">
                      <?php pll_e('bilddare'); ?>
                  </h2>
              </div>
              <div class="pf_row address-box dep--form-block">
                  <div>
                      <div class="lab dep--small-grey field__label">
                          <label for="BillingAddress1">
                              <?php pll_e('Address'); ?></label>
                      </div>
                      <div class="field">
                          <input type="text" class="depFieldFocus dep--input" id="BillingAddress1" name="BillingAddress1" data-form="BillingAddress1" placeholder="<?php pll_e('Address'); ?>" maxlength="100" value="<?php echo $lane; ?>">
                          <div id="BillingAddress1_error" class="error-box">
                              <?php pll_e('plenyouadddre'); ?>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="pf_row address-box dep--form-block">
                  <div>
                      <div class="lab dep--small-grey">
                          <label for="BillingCountry">
                              <?php pll_e('Country'); ?></label>
                      </div>
                      <select id="BillingCountry" class="brf--select" name="BillingCountry">
                          <?php echo $countries ?></select>
                  </div>
              </div>
              <div class="pf_row address-box dep--form-block">
                  <div class="city_zip_box">
                      <div class="box1">
                          <div class="lab dep--small-grey field__label">
                              <label for="BillingCity">
                                  <?php pll_e('City'); ?></label>
                          </div>
                          <div class="field">
                              <input type="text" class="depFieldFocus dep--input" id="BillingCity" name="BillingCity" placeholder="<?php pll_e('City'); ?>" maxlength="60" value="<?php echo $city; ?>">
                              <div id="BillingCity_error" class="error-box">
                                  <?php pll_e('plencity'); ?>
                              </div>
                          </div>
                      </div>
                      <div class="box_sd">
                          <div class="lab dep--small-grey field__label">
                              <label for="BillingZipCode">
                                  <?php pll_e('Postal/Zip Code'); ?></label>
                          </div>
                          <div class="field">
                              <input type="text" class="depFieldFocus dep--input" id="BillingZipCode" name="BillingZipCode" data-form="BillingZipCode" placeholder="<?php pll_e('Postal/Zip Code'); ?>" maxlength="15" value="<?php echo $code; ?>">
                              <div id="BillingZipCode_error" class="error-box">
                                  <?php pll_e('plenzip'); ?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <?php
            $saved1 = '';
            $saved2 = '';
            if ($_SESSION['rememberBilling']){
              $saved1 = 'checked';
              $saved2 = 'check';
            }
          ?>
              <!-- Save Billing Address -->
              <label class="checkbox-field save-billing">
                  <?php pll_e('svbiladdre'); ?>
                  <input id="bill__checkbox" name="chcustom" type="checkbox" <?php echo $saved1 ?>>
                  <span class="checkmark"></span>
              </label>
          </fieldset>
      </div>
      <div class="depDepTermsChkBox dep--form-block chcustom_terms">
        <!--checkbox-->
        <div class="depCheckbox">
          <input type="checkbox" id="ReviewtermsConditionsAgree" name="termsConditionsAgree" value="1">
        </div>
        <!-- Terms second page -->
        <label class="checkbox-field save-billing">
          By clicking 'Deposit' you are indicating that you have read and agreed to <a href="/regulatory-documents/terms-and-conditions" target="_blank">Terms and Conditions</a> and <a href="/regulatory-documents/privacy-policy" target="_blank">Privacy Policy</a>
          <input name="chcustom" type="checkbox" id="terms" data-form="terms">
          <span class="checkmark"></span>
        </label>
        <div class="depDepRevTermErr">
          Please agree to Terms and Conditions            
        </div>
      </div>
      <div class="pf_row btn" id="contButtonWrap" style="margin-bottom:20px;">
          <div class="lab dep--small-grey"></div>
          <div class="depFirstSubmitCont field dep--btn--blue">
              <div class="depPreloaderCirclesPopup depDepPreloader adf_DepositAn" style="left: -15px;">
                  <div class="depPreloaderCrlCont">
                      <div class="depPreloaderCircle depCircleBlue" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div>
                  </div>
                  <div class="depPreloaderCrlCont">
                      <div class="depPreloaderCircle depCirleGreen" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div>
                  </div>
                  <div class="depPreloaderCrlCont">
                      <div class="depPreloaderCircle depCircleRed" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div>
                  </div>
              </div>
              <span id="orangeRedTxt" style="display: none; position: absolute; margin-top: 40px;">Please, wait a few seconds. You will be redirected to the payment form page.</span>
              <!-- Submit -->
              <span class="button-border">
                  <a id="deposit__submit" href="#" class="button button-xs sb-btn"><span>Deposit</span></a>
              </span>
              <div class="sb-submit_error">
                  <?php pll_e('plchinfi'); ?>
              </div>
          </div>
      </div>
      <input type="hidden" id="termsConditions" name="termsConditions" value="1" />
  </form>
  <!--
    END OF NETPAY FORM
   -->


</div>

<!-- BANKWIRE "FORM" -->
<div class="bankwirebox" id="bankwire">
  <p><?php pll_e('fordetconw'); ?></p>
</div>