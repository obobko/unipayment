<!--ORANGE FORM-->
<?php 
include( plugin_dir_path( __FILE__ ) . 'form-header.php');    
?>
<div class="adfFormsWrap">
  <div id="adf_redir_cc" style="display: none;">You will be redirected to the payment form page</div> <!--orange redirect to the cc form-->
  <form method="POST" class="unipayForm" data-form="nextpay" action="javascript:void(null);">
    <input type="hidden" class="adf_selected_form" value="<?php echo $this->adf_form_default; ?>" /><!--selected form-->
    <input type="hidden" id="u_email" value="<?php echo $_SESSION['email']; ?>">
    <input type="hidden" class="adf_url" value="<?php echo admin_url( 'admin-ajax.php' ) ?>">
   
    <div>
        <fieldset class="personalInfo">
      <div class="brfx--form-block-title">
        <div class="brfx--icon--moneyup brf--float-left brfx-icon-form"></div>
        <h2 class="brfx--font-h2 brfx--font-h2-x"><?php pll_e('Funds'); ?></h2>
      </div>
      <?php
        $firstdep = '';
        if ($_SESSION['firstDepositDate'] == ''){
           $firstdep = '<p class="fundsP">'.pll__('thecurwillfix').'</p>';
        }
        _e($firstdep,  'unipayForm');
      ?>
      <div class="brfx--form-block brfxDepFloatBoxWrap brfxOverflow">
        <div class="box1 brfxDepFloatBox">
          <div class="lab brfx--small-grey">
            <label for="Amount"></label>
          </div>
          <div class="field">
            <input value="" type="number" class="depFieldFocus brfx--input" id="Amount" name="Amount" data-form="Amount" placeholder="<?php pll_e('Enter amount'); ?>" maxlength="10">
            <div id="Amount_error" class="error-box"><?php pll_e('pl-en-valamount'); ?></div>
          </div>
        </div>
        <div class="pf_row box_sd brfxDepFloatBox">
          <div class="lab brfx--small-grey">
            <label for="Currency"></label> 
          </div>
          <div class="field">
            <?php
              $currency = '';
              if ($_SESSION['firstDepositDate'] != ''){
                 $currency = 'disabled';
              }
              $c_ch1 = '';
              $c_ch2 = '';
              
              if( $_SESSION['firstDepositDate'] == ''){
                $c_ch2 = 'selected';
              }else{
                if ($user_info['currency'] == 'USD'){
                  $c_ch1 = 'selected';
                }else{
                  $c_ch2 = 'selected';
                } 
              }
              
              $currency_options = '<option value="USD" '.$c_ch1.'>USD</option><option value="EUR" '.$c_ch2.'>EUR</option>';
            ?>
            <select class="brf--select" name="Currency" id="Currency" <?php echo $currency; ?>>
              <?php echo $currency_options; ?>
            </select>
          </div>
        </div>
        <div class="balancebox brfxDepFloatBox">
        <p><?php pll_e('Current balance'); ?>: <span class="current_balance"><?php echo $form_balance ?></span> <span class="current_balance_currency"><?php 
    
        if($_SESSION['firstDepositDate'] == ''){
          echo 'EUR'; 
        }else{
          echo $user_info['currency'];
        }
          
          ?></span></p>
        <p><?php pll_e('balafterdep'); ?>: <span class="balance_after_deposit"><?php echo $form_balance ?></span> <span class="balance_after_currency"><?php 
        if($_SESSION['firstDepositDate'] == ''){
          echo 'EUR'; 
        }else{
          echo $user_info['currency'];
        }
          ?></span></p>
        </div>
      </div>
     </fieldset>
    </div>
      
   
    
    <div class="pf_row btn" id="contButtonWrap" style="margin-bottom:20px;">
      <div class="lab brfx--small-grey"></div>
      <div class="brfxFirstSubmitCont field brfx--btn--blue">
        <div class="brfxPreloaderCirclesPopup brfxDepPreloader adf_DepositAn" style="left: -15px;">
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCircleBlue" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div></div>
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCirleGreen" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div></div>
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCircleRed" style="width: 24px; height: 24px; top: 30px; left: 30px;"></div></div>
        </div>
        <span id="orangeRedTxt" style="display: none; position: absolute; margin-top: 40px;">Please, wait a few seconds. You will be redirected to the payment form page.</span>
        <input class="pf_pm_btn sb-btn" type="submit" value="<?php pll_e('Continue'); ?>" />
        <div class="sb-submit_error"><?php pll_e('plchinfi'); ?></div>
      </div>
    </div>

    <input type="hidden" id="termsConditions" name="termsConditions" value="1" />

  </form>
</div>

<?php 
include( plugin_dir_path( __FILE__ ) . 'form-footer.php');    
?>