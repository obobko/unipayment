<div class="unipayConditions">

  <div class="unipayConditionsHead">
    <h1><?php pll_e('Payment review'); ?></h1>
    <p><?php pll_e('makesurecor'); ?></p>
  </div>

  <div class="adddepcont">
    <div class="review-box">
      <div class="PersonalInformationBox">
        <h2 class="section-title"><?php pll_e('Funds'); ?></h2>
        <div class="fund_cur_box brfxOverflow">
          <div class="depReviewRow">
            <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Amount'); ?></div>
            <div class="funds brfxReviewTxt brfxfloatLeft"></div>
            <div class="currency brfxReviewTxt brfxfloatLeft"></div>
          </div>  
        </div>
      </div>
      <div class="CreditCardDetailsBox brfxOverflow">
        <h2 class="section-title"><?php pll_e('crcainfo'); ?></h2>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Name on Card'); ?></div>
          <div class="member brfxReviewTxt brfxfloatLeft"></div>
        </div>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Card Number'); ?></div>
          <div class="cardNum  brfxReviewTxt brfxfloatLeft"></div>
        </div>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Expiry Date'); ?></div>
          <div class="dataexp brfxReviewTxt brfxfloatLeft"></div>
        </div>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('CVV'); ?></div>
          <div class="cvv brfxReviewTxt brfxfloatLeft"></div>
        </div>
      </div>
      <div class="BillingAddressBox brfxOverflow">
        <h2 class="section-title"><?php pll_e('bilddare'); ?></h2>

        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Address'); ?></div>
          <div class="address brfxReviewTxt brfxfloatLeft"></div>
        </div>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Country'); ?></div>
          <div class="country brfxReviewTxt brfxfloatLeft"></div>
        </div>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('City'); ?></div>
          <div class="city brfxReviewTxt brfxfloatLeft"></div>
        </div>
        <div class="depReviewRow">
          <div class="brfxReviewName brfxfloatLeft"><?php pll_e('Zip Code'); ?></div>
          <div class="zip brfxReviewTxt brfxfloatLeft"></div>
        </div>
      </div>

    </div>
    <div class="brfxDepTermsChkBox brfx--form-block chcustom_terms">
      <!--checkbox-->
      <div class="brfxCheckbox">      
        <input type="checkbox" id="ReviewtermsConditionsAgree" name="termsConditionsAgree"  value="1">
        <label for="ReviewtermsConditionsAgree"><span></span></label>
      </div>
      <p><?php pll_e('byclickdep'); ?></p>
      <div class="brfxDepRevTermErr"><?php pll_e('terms-agree-error-dep'); ?></div>
    </div>

    <div class="review_btn_container ">
      <div class="editDataDeposit">
        <div class="editdataDep pf_pm_btn sb-btn brfx--btn--blue"><a href="#"><?php pll_e('Edit-btn'); ?></a></div>
      </div>

      <div class="cond_btn">
        <div class="brfxReviwBoxDepositBtn pf_pm_btn sb-btn brfx--btn--blue">
          <div class="brfxPreloaderCirclesPopup brfxDepPreloader adf_DepositAn">
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCircleBlue"></div></div>
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCirleGreen"></div></div>
            <div class="brfxPreloaderCrlCont"><div class="brfxPreloaderCircle brfxCircleRed"></div></div>
          </div>
          <a href="#"><?php pll_e('Deposit'); ?></a>
        </div>
      </div>
    </div>
  </div>
    
</div>

<div class="bankwirebox">
  <p><?php pll_e('fordetconw'); ?></p>
</div>

<div class="paymentResultBox">

  <div class="deposit_page d_p3">
    <div class="d_success">
      <div class="d_sm">
        <i class="fa fa-usd" aria-hidden="true"></i>
        <h2 class="balanceDepSucc"><?php pll_e('depwassuc'); ?></h2>
        <h2 class="balanceDepSucc balanceDepSuccBalance"><?php pll_e('Your Balance'); ?>: <span class="d_new_balance"></span></h2>
      </div>
      <div class="d_sm2">
        <p><?php pll_e('confemsent'); ?></p>
        <p><?php pll_e('Transaction ID'); ?>: <span class="d_transaction_id"></span></p>
      </div>
    </div>
    <div class="d_links">
      <div class="d_center_container">
        <h2><?php pll_e('start trading now'); ?></h2>
      </div>
      <div class="d_linksWrap brfxOverflow">
        <div class="brfx--btn--blue brfxfloatLeft"><a href="https://download.mql5.com/cdn/web/br.consulting.group/mt5/brconsulting5setup.exe"><?php pll_e('download mt5'); ?></a></div>
        <div class="brfx--btn--blue brfxfloatLeft"><a href="/web_trader/"><?php pll_e('open web trader'); ?></a></div>
      </div>
    </div>
  </div>

</div>