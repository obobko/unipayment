<?php 

if ( ! defined( 'ABSPATH' ) ){
	exit;
}

function curl_cur($url)
{
	$ch = curl_init();
    $timeout = 0;
	 curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt ($ch, CURLOPT_USERAGENT,
                 "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
	return $rawdata;
}

function get_currency_google($from_Currency, $to_Currency, $amount) {
    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);

    $url = "finance.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

    $rawdata = curl_cur($url);
   
    $data = explode('bld>', $rawdata);
    $data = explode($to_Currency, $data[1]);
	
	return floatval($data[0]);
}

function get_currency_fixer($from_Currency, $to_Currency)
{
	$rawdata  = curl_cur('http://api.fixer.io/latest?symbols='.$from_Currency.','.$to_Currency);
	$rawdata = json_decode($rawdata);
	return $rawdata->rates->USD;
}

function get_currency_floatrates($from_Currency)
{
	$rawdata  = curl_cur('http://www.floatrates.com/daily/'.strtolower($from_Currency).'.json');
	$rawdata = json_decode($rawdata);
	return $rawdata->usd->rate;
}

function getRate()
{
	$result = 0;
	$google = get_currency_google('EUR', 'USD', 1);
	if($google != '' && is_numeric($google) && $google!=0){
		$result = $google;
	}else{
//		$fixer = get_currency_fixer('EUR', 'USD');
//		if($fixer != '' && is_numeric($fixer)){
//			$result = $fixer;
//		} else {
			$floatrates = get_currency_floatrates('EUR');
			if($floatrates != '' && is_numeric($floatrates)){
				$result = $floatrates;
			}else{
				$result = 'No data';
			}
//		}
	}
	return $result;
}

?>