# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-29 14:34+0300\n"
"PO-Revision-Date: 2017-10-31 15:09+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.0.4\n"
"Language-Team: \n"
"Last-Translator: \n"
"Language: sv\n"

#: includes/forms/form.php:51 includes/forms/form2.php:52
msgid "Payment method"
msgstr "BETALNINGSMETOD"

#: includes/forms/form2.php:55
msgid "Credit Card"
msgstr "Kreditkort"

#: includes/forms/form2.php:56
msgid "Bank Wire"
msgstr "Banköverföring"

#: includes/forms/form2.php:69 includes/forms/form2.php:230
msgid "Billing Address"
msgstr "FAKTURAADRESS"

#: includes/forms/form2.php:74
msgid "Address"
msgstr "Adress"

#: includes/forms/form2.php:78
msgid "Please enter your Address"
msgstr "Vänligen ange din adress"

#: includes/forms/form2.php:86
msgid "Country"
msgstr "Valuta"

#: includes/forms/form2.php:96
msgid "City"
msgstr "Stad"

#: includes/forms/form2.php:100
msgid "Please enter your city"
msgstr "Ange din stad"

#: includes/forms/form2.php:105
msgid "Zip Code"
msgstr "Postnummer"

#: includes/forms/form2.php:109
msgid "Please enter valid zip code"
msgstr "Vänligen ange giltigt postnummer"

#: includes/forms/form2.php:119
msgid "Save billing address"
msgstr "Spara faktureringsadress"

#: includes/forms/form2.php:127
msgid "Credit card information"
msgstr "KREDITKORTSINFORMATION"

#: includes/forms/form2.php:130
msgid "Name on Card"
msgstr "Namn på kort"

#: includes/forms/form2.php:134
msgid "Please enter your Name"
msgstr ""

#: includes/forms/form2.php:139
msgid "Card Number"
msgstr "Kortnummer"

#: includes/forms/form2.php:143
msgid "Please enter valid Card Number"
msgstr "Ange giltig kortnummer"

#: includes/forms/form2.php:149
msgid "Expiry Date"
msgstr "Utgångsdatum"

#: includes/forms/form2.php:161
msgid "Please enter valid CVV"
msgstr "Ange giltig CVV"

#: includes/forms/form2.php:166
msgid "We do not store your credit card information"
msgstr "Vi lagrar inte din kreditkortsinformation"

#: includes/forms/form2.php:173 includes/forms/form2.php:247
msgid "Funds"
msgstr "SALDO"

#: includes/forms/form2.php:179
msgid "Amount"
msgstr "Mängd"

#: includes/forms/form2.php:183
msgid "Please enter valid Amount"
msgstr "Ange giltigt belopp"

#: includes/forms/form2.php:188
msgid "Currency"
msgstr "Valuta"

#: includes/forms/form2.php:199
msgid "Current balance"
msgstr "Aktuellt saldo"

#: includes/forms/form2.php:200
msgid "Balance after deposit"
msgstr "Saldo efter insättning"

#: includes/forms/form2.php:209
msgid "Continue"
msgstr "Fortsätt"

#: includes/forms/form2.php:222
msgid "Payment review"
msgstr "BETALNINGSÖVERSIKT"

#: includes/forms/form2.php:223
msgid "Make sure all the fields are correct"
msgstr "Se till att alla fält är korrekta"

#: includes/forms/form2.php:239
msgid "Credit Card Information"
msgstr "KREDITKORTSINFORMATION"

#: includes/forms/form2.php:266
msgid "Edit"
msgstr "Redigera"

#: includes/forms/form2.php:270
msgid "Deposit"
msgstr "INSÄTTNING"

#: includes/forms/form2.php:278
msgid "Bank wire"
msgstr "Banköverföring"

#: includes/forms/form2.php:288
msgid "the deposit was successful"
msgstr "insättning genomförd"

#: includes/forms/form2.php:292
msgid "Confirmation mail was send to your email address"
msgstr "Bekräftelsemail skickades till din e-postadress"

#: includes/forms/form2.php:298
msgid "start trading now"
msgstr "börja handla idag"

#: includes/forms/form2.php:300
msgid "download mt5"
msgstr "ladda ner mt5"

#: includes/forms/form2.php:301
msgid "open web trader"
msgstr "öppen webbhandlare"
