# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-29 14:34+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: includes/forms/form.php:51 includes/forms/form2.php:52
msgid "Payment method"
msgstr ""

#: includes/forms/form2.php:55
msgid "Credit Card"
msgstr ""

#: includes/forms/form2.php:56
msgid "Bank Wire"
msgstr ""

#: includes/forms/form2.php:69 includes/forms/form2.php:230
msgid "Billing Address"
msgstr ""

#: includes/forms/form2.php:74
msgid "Address"
msgstr ""

#: includes/forms/form2.php:78
msgid "Please enter your Address"
msgstr ""

#: includes/forms/form2.php:86
msgid "Country"
msgstr ""

#: includes/forms/form2.php:96
msgid "City"
msgstr ""

#: includes/forms/form2.php:100
msgid "Please enter your city"
msgstr ""

#: includes/forms/form2.php:105
msgid "Zip Code"
msgstr ""

#: includes/forms/form2.php:109
msgid "Please enter valid zip code"
msgstr ""

#: includes/forms/form2.php:119
msgid "Save billing address"
msgstr ""

#: includes/forms/form2.php:127
msgid "Credit card information"
msgstr ""

#: includes/forms/form2.php:130
msgid "Name on Card"
msgstr ""

#: includes/forms/form2.php:134
msgid "Please enter your Name"
msgstr ""

#: includes/forms/form2.php:139
msgid "Card Number"
msgstr ""

#: includes/forms/form2.php:143
msgid "Please enter valid Card Number"
msgstr ""

#: includes/forms/form2.php:149
msgid "Expiry Date"
msgstr ""

#: includes/forms/form2.php:161
msgid "Please enter valid CVV"
msgstr ""

#: includes/forms/form2.php:166
msgid "We do not store your credit card information"
msgstr ""

#: includes/forms/form2.php:173 includes/forms/form2.php:247
msgid "Funds"
msgstr ""

#: includes/forms/form2.php:179
msgid "Amount"
msgstr ""

#: includes/forms/form2.php:183
msgid "Please enter valid Amount"
msgstr ""

#: includes/forms/form2.php:188
msgid "Currency"
msgstr ""

#: includes/forms/form2.php:199
msgid "Current balance"
msgstr ""

#: includes/forms/form2.php:200
msgid "Balance after deposit"
msgstr ""

#: includes/forms/form2.php:209
msgid "Continue"
msgstr ""

#: includes/forms/form2.php:222
msgid "Payment review"
msgstr ""

#: includes/forms/form2.php:223
msgid "Make sure all the fields are correct"
msgstr ""

#: includes/forms/form2.php:239
msgid "Credit Card Information"
msgstr ""

#: includes/forms/form2.php:266
msgid "Edit"
msgstr ""

#: includes/forms/form2.php:270
msgid "Deposit"
msgstr ""

#: includes/forms/form2.php:278
msgid "Bank wire"
msgstr ""

#: includes/forms/form2.php:288
msgid "the deposit was successful"
msgstr ""

#: includes/forms/form2.php:292
msgid "Confirmation mail was send to your email address"
msgstr ""

#: includes/forms/form2.php:298
msgid "start trading now"
msgstr ""

#: includes/forms/form2.php:300
msgid "download mt5"
msgstr ""

#: includes/forms/form2.php:301
msgid "open web trader"
msgstr ""
