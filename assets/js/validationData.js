var adf_terms_popup_show = false; //false - without popup terms and conditions

(function($) {
  // GLOBAL VARIABLES
  var depPreloaderIntervalPopup;
  var send_deposit_data = {};
  var adf_selected_form = "bridget1";

  //show deposit animation
  function depPreloaderPopup(elem) {
    $("."+elem).fadeIn(1000, "swing");
    setTimeout(function() {
      deploaderAnimation("depCircleBlue");
    }, 1000);
    setTimeout(function() {
      deploaderAnimation("depCirleGreen");
    }, 2000);
    setTimeout(function() {
      deploaderAnimation("depCircleRed");
    }, 3000);
  }
  
  //hide deposit animation
  function depHidePreloaderPopup() {
    $(".adf_DepositAn").hide();
    clearInterval(depPreloaderIntervalPopup);
  }
  
  //payment method click
  $(document).on('click', '[data-type]', function(e){
    e.preventDefault();

    //switch form
    $('.addDepositForm').hide();
    adf_selected_form  = $(this).data("form");
    $("#"+adf_selected_form).show();

    //pretty border
    $('.pm-wrap').removeClass('pm-btn-active');
    $(this).addClass('pm-btn-active');
  })
  
  //DEPOSIT BUTTON
  function adf_validate() {
    send_deposit_data = {}; //form data
    var val_array = []; //validation result
    switch (adf_selected_form) {
      case "bridget1":
        var form = $("#"+adf_selected_form);
        
        send_deposit_data["action"] = "paymentBridger";
        var adf_Amount = form.find("#Amount-quick");
        send_deposit_data["Amount"] = adf_Amount.val();
        val_array.push(adf_validation(adf_Amount));
        var adf_Currency = form.find("#Currency");
        send_deposit_data["Currency"] = adf_Currency.val();
        send_deposit_data["Comment"] = "";
        
        var adf_billingAddress1 = form.find("#BillingAddress1");
        send_deposit_data["BillingAddress1"] = adf_billingAddress1.val();
        val_array.push(adf_validation(adf_billingAddress1));

        var adf_BillingCity = form.find("#BillingCity");
        send_deposit_data["BillingCity"] = adf_BillingCity.val();
        val_array.push(adf_validation(adf_BillingCity));

        var adf_BillingZipCode = form.find("#BillingZipCode");
        send_deposit_data["BillingZipCode"] = adf_BillingZipCode.val();
        val_array.push(adf_validation(adf_BillingZipCode));
        
        if (form.find("#bill__checkbox").is(":checked")) {
          send_deposit_data["SaveBilling"] = "save";
        } else {
          send_deposit_data["SaveBilling"] = "dont_save";
        }

        var adf_BillingCountry = form.find("#BillingCountry");
        send_deposit_data["BillingCountry"] = adf_BillingCountry.val();
        
        var adf_Terms = form.find("#terms");
        val_array.push(adf_validation(adf_Terms));
      break;
        
      case "form-quick":
        send_deposit_data["region"] = "quick"; //Region deposit type INT or EU
        var form = $("#"+adf_selected_form);
        
        send_deposit_data["action"] = "paymentNetpay";
        
        var adf_Amount = form.find("#Amount-quick");
        send_deposit_data["Amount"] = adf_Amount.val();
        val_array.push(adf_validation(adf_Amount));

        var adf_Currency = form.find("#Currency");
        send_deposit_data["Currency"] = adf_Currency.val();

        send_deposit_data["Comment"] = "FXM Capital payment page";

        var adf_member = form.find("#Member");
        send_deposit_data["Member"] = adf_member.val();
        val_array.push(adf_validation(adf_member));

        var adf_CardNum = form.find("#CardNum");
        send_deposit_data["CardNum"] = adf_CardNum.val().replace(/-|\s/g, "");
        val_array.push(adf_validation(adf_CardNum));

        var adf_CVV2 = form.find("#CVV2");
        send_deposit_data["CVV2"] = adf_CVV2.val();
        val_array.push(adf_validation(adf_CVV2));

        var adf_billingAddress1 = form.find("#BillingAddress1");
        send_deposit_data["BillingAddress1"] = adf_billingAddress1.val();
        val_array.push(adf_validation(adf_billingAddress1));

        var adf_BillingCity = form.find("#BillingCity");
        send_deposit_data["BillingCity"] = adf_BillingCity.val();
        val_array.push(adf_validation(adf_BillingCity));

        var adf_BillingZipCode = form.find("#BillingZipCode");
        send_deposit_data["BillingZipCode"] = adf_BillingZipCode.val();
        val_array.push(adf_validation(adf_BillingZipCode));

        if (form.find("#bill__checkbox").is(":checked")) {
          send_deposit_data["SaveBilling"] = "save";
        } else {
          send_deposit_data["SaveBilling"] = "dont_save";
        }

        var adf_ExpYear = form.find("#ExpYear");
        send_deposit_data["ExpYear"] = adf_ExpYear.val();

        var adf_ExpMonth = form.find("#ExpMonth");
        send_deposit_data["ExpMonth"] = adf_ExpMonth.val();

        var adf_BillingCountry = form.find("#BillingCountry");
        send_deposit_data["BillingCountry"] = adf_BillingCountry.val();
        
        var adf_Terms = form.find("#terms");
        val_array.push(adf_validation(adf_Terms));
      break;
    }

    for (var i = 0; i < val_array.length; i++) {
      if (!val_array[i]) {
          $("#form_standart .sb-submit_error").addClass("sb-submit_error_show");
          setTimeout(function() {
            $("#form_standart .sb-submit_error").removeClass("sb-submit_error_show");
          }, 3000);
        return;
      }
    }
    
    switch (adf_selected_form) {
      case "form-quick":
        //adf_dataSendReview(); //show netpay review page
        adf_SendPayData();
        break;
      case "bridget1":
        adf_SendPayData_bridget1();
        break;  
    }
  }

  //VALIDATION
  function adf_validation(field) {

    var subject = $("#"+adf_selected_form).find(field);
    var error = "";
    var data_form = subject.attr("data-form");

    if (subject.val() == "") {
      // if empty
      error = true;
    }

	  var depmin = $('#depmin').attr("value");
	
    if (data_form == "Amount" && (subject.val() < parseInt(depmin)) && error != true) {
      error = true;
    }

    if ( data_form == "Amount" && error != true ) {
      //if not number and not 0
      if (isNaN(subject.val()) || /^0*$/.test(subject.val())) {
        error = true;
      }
    }

    if (data_form == "CardNum" && error != true) {
      if (subject.val().match(/[a-z]/i)) {
        //if has letters in it
        error = true;
      }
      if (!adf_detectCreditCard(subject.val().replace(/-|\s/g, ""))) {
        //if number visa, master card, jsb
        error = true;
      }
      if (subject.val().replace(/-|\s/g, "").length < 13) {
        //if smaller then 13 digits
        error = true;
      }
    }
    
    if (data_form == "terms" && error != true){
      if (!subject.is(":checked")) {
        $("#"+adf_selected_form).find(".depDepRevTermErr").show();
        setTimeout(function(){  $("#"+adf_selected_form).find(".depDepRevTermErr").hide() }, 3000);
        error = true;
      }      
    }

    if (error) {
      subject.removeClass("adf_valid");
      if (!subject.hasClass("adf_error")) {
        subject.addClass("adf_error");
      }
      subject.next(".error-box").attr("style", "visibility: visible;");
      subject.parent().parent().find(".dep_inf_bx_win").toggle();

      setTimeout(function() {
        subject.next(".error-box").attr("style", "visibility: hidden;");
        subject.parent().parent().find(".dep_inf_bx_win").toggle();
      }, 3000);

      return false;
    } else {
      subject.removeClass("adf_error");
      if (!subject.addClass("adf_valid")) {
        subject.addClass("adf_valid");
      }
      return true;
    }
  }
  
  // SEND DATA
  function adf_SendPayData_bridget1(){
    console.log("send data") 
    console.log(send_deposit_data) 
    btn__anim("#bridget1_deposit__submit")
    //get redirect ajax  
    $.ajax({
      url: $(".adf_url").val(),
      type: 'POST',
      dataType: 'json',
      data: send_deposit_data,
      success: function(data){
        if(!data.result){
          btn_anim__remove("#bridget1_deposit__submit")
          adf_popupError(data.message+" "+data.response); //show error
        }
        else {
          window.location.replace(data.url);
          btn_anim__remove("#bridget1_deposit__submit");
//          adf_successWindow(data.amount, data.currency, data.transid);
        }
      }
    })
  }

  function adf_SendPayData() {
    btn__anim("#deposit__submit")
      
    $.ajax({
      url: $(".adf_url").val(),
      type: 'GET',
      dataType: 'json',
      data: send_deposit_data,
      success: function(data){
        
        if ( data.Redirected == "1" && data.Reply == "553" ) { //if 3d redirect in not empty
          btn_anim__remove("#deposit__submit");
          netpayAuthError(data.D3Redirect); // show auth error popup and 3dredirect
        } 
        else if(data.Redirected == "1" && data.Reply != "553") { //error popup show
          btn_anim__remove("#deposit__submit");
          $(".depReviwBoxDepositBtn a").removeClass("depBtnHide");
          adf_popupError(data.ReplyDesc); //show error
        }
        else if(data.Redirected == "0" && data.Reply == "553") { //redirect only
          popitup(data.D3Redirect);
        }
        else if(data.Redirected == "0" && data.Reply == "000"){ //success popup
          btn_anim__remove("#deposit__submit");
          adf_successWindow(data.Amount, data.Currency, data.TransID);
        } 
        else if(data.Redirected == "0" && data.Reply != "000"){ //success popup
          btn_anim__remove("#deposit__submit");
          $(".depReviwBoxDepositBtn a").removeClass("depBtnHide");
          adf_popupError(data.ReplyDesc); //show error
        }
		else
			alert(data.message);
      }
    })

  }
  
  //Netpay hidden form
  function netpayCreateHiddenForm(data){
    var form = ""
    //check if form already exists
    if($('#netPayFormHdn').length) $('#netPayFormHdn').remove()
    //create nextpay form
    form += '<form id="netPayFormHdn" method="post" action="https://uiservices.netpay-intl.com/hosted/">';
    for(key in data)
    {
		if(key == 'result')
			continue;
      form +='<input name="'+key+'" type="hidden" value="'+data[key]+'" />';
    }
    form += '</form>'
    $(".adfFormsWrap").append(form)
    $("#netPayFormHdn").submit()
  }


  //3d REDIRECT WHEN AUTHORIZATION FAILED
  function netpayAuthError(url){
    var dRedirect = url;
    btn__anim("#auth__err"); //show preloader
    $('.depAuthErrorPopup').show(); //show auth error popup

    setTimeout(function(){
      popitup(dRedirect); //redirect
    }, 15000); //after 15 second redirect
  }

  //succes dep popup show
  function adf_successWindow(Balance, Currency, id) {
    if(Currency == '1'){
      var f_currency = 'USD';
    }else{
      var f_currency = 'EUR';
    }
    $('.d_transaction_id').html(id);
    $('.d_new_balance').html(Balance+' '+f_currency);
    $('.depDepSuccPopup').show(); //show popup succes dep
  }

  
  function adf_startPreloader(elem) {
    setTimeout(function(){
      depPreloaderPopup(elem);
    }, 500);
    depPreloaderIntervalPopup = setInterval(
      function(){
      depPreloaderPopup(elem);
      }, 
      3000);
  }

  function popitup(url) {
    window.open(url, "_self", false);
  }

  //this one shows error
  function adf_popupError(ReplyDesc) {
    if (adf_terms_popup_show && $(".TOCcontenttoshow").length != 0) {
//      adf_popupTermsHide();
    }
    $(".depDepErrorPopup .modal_text").html(ReplyDesc);
    $(".depDepErrorPopup").show();
  }

  function adf_scroll_top() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
  }

  // misk
  function getSelectedText() {
    var elt;
    if( $( '.pw-dw__item-radio-active' ).data( 'deposit' ) == "quick" )
    {
      elt = document.querySelector(".currency-quick");
    }
    else 
    {
      elt = document.getElementById("Currency");
    }
    if (elt.selectedIndex == -1) {
      return null;
    }
    return elt.options[elt.selectedIndex].text;
  }

  //CONTINUE BUTTON CLICK
  $(document).on("click", "#deposit__submit, #bridget1_deposit__submit", function(e) {
    e.preventDefault();
    adf_validate();
  });

  // validate on blur
  $(document).on("blur", ".depFieldFocus", function(e) {
    var subject = $(this);
    if(subject.attr('id') != "CVV2"){
      subject.val($.trim(subject.val().replace(/\s{2,}/g, " ")));
    }
    adf_validation(subject);
    $(this).parent().parent().find('.lab').addClass('field__label');
  });

  function adf_updateBalAftDep(val) {
    var current_balance;
   
      if( $( '.pw-dw__item-radio-active' ).data( 'deposit' ) == "quick" )
      {
         current_balance = Number($("#form-quick .current_balance").html());              
      }
      else
      {
        current_balance = Number($("#form_standart .current_balance").html());
      } 
    var balance_after_deposit = current_balance + val;

      if( $( '.pw-dw__item-radio-active' ).data( 'deposit' ) == "quick" )
      {
        $("#form-quick .balance_after_deposit").html(balance_after_deposit);
        $("#form-quick .balance_after_currency").html(getSelectedText());
      }
      else 
      {
        $("#form_standart .balance_after_deposit").html(balance_after_deposit);
        $("#form_standart .balance_after_currency").html(getSelectedText());
      }
  }

  function adf_updateCurrAftDep() {
      if( $( '.pw-dw__item-radio-active' ).data( 'deposit' ) == "quick" )
      {
        $("#form-quick .balance_after_currency").html(getSelectedText());
      }
      else
      {
        $("#form_standart .balance_after_currency").html(getSelectedText());
      }
  }
  // update 'Balance after deposit' on Amount input
  $(document).on("input", "#Amount, #Amount-quick", function() {
    var val = Number(this.value);
    adf_updateBalAftDep(val);
  });

  // update 'Balance after deposit Currency' on currency change
  $(document).on("change", "#Currency, .currency-quick", function() {
    adf_updateCurrAftDep();
  });

  //EDIT BUTTON CLICK FROM POPUP
  $(".edit_dep__2p").click(function(e) {
    e.preventDefault();
    $( ".addDepositForm, #form_standart" ).hide()
    if(adf_selected_form == 'netpay'){
      $('.column-dash').removeAttr("style");
      $("#popup_dep__error").hide();
      
      $(".depCreditTypesBox").show();
      $("#form_standart").show();
      $(".addDepositConditions").attr("style", "display: none;");
      $(".dep-form-head").attr("style", "display: block;");
      $("hr.line_fst").attr("style", "display: block;");
      if ($(".depReviwBoxDepositBtn a").hasClass("depNotActiveBtn")) {
        $(".depReviwBoxDepositBtn a").removeClass("depNotActiveBtn");
      }
      $( "#form_standart" ).hide()
      $("#form-quick").show();
    }else {
      $('.column-dash').removeAttr("style");
      $("#popup_dep__error").hide();
      
      $(".depCreditTypesBox").show();
      $("#form_standart").show();
      $(".addDepositConditions").attr("style", "display: none;");
      $(".dep-form-head").attr("style", "display: block;");
      $("hr.line_fst").attr("style", "display: block;");
      if ($(".depReviwBoxDepositBtn a").hasClass("depNotActiveBtn")) {
        $(".depReviwBoxDepositBtn a").removeClass("depNotActiveBtn");
      }
      $( "#form_standart" ).hide()
    }
    $( "#form_standart" ).hide()
  });


  function adf_SendQuickPayData()
  {
    var elem = "#adf_submit_dep_btn";
    $(".depReviwBoxDepositBtn a").addClass("depBtnHide");
    $.post($(".adf_url").val(), send_deposit_data, function(data) {
        adf_dep_btn_validate(elem); //end animation

        if ( data.Redirected == "1" && data.Reply == "553" ) { //if 3d redirect in not empty
          btn_anim__remove("#deposit_dep_2p");
          netpayAuthError(data.D3Redirect); // show auth error popup and 3dredirect
        } 
        else if(data.Redirected == "1" && data.Reply != "553") { //error popup show
          btn_anim__remove("#deposit_dep_2p");
          $(".depReviwBoxDepositBtn a").removeClass("depBtnHide");
          adf_popupError(data.ReplyDesc); //show error
        }
        else if(data.Redirected == "0" && data.Reply == "553") { //redirect only
          popitup(data.D3Redirect);
        }
        else if(data.Redirected == "0" && data.Reply == "000"){ //success popup
          btn_anim__remove("#deposit_dep_2p");
          adf_successWindow(data.Amount, data.Currency, data.TransID);
        } 
        else if(data.Redirected == "0" && data.Reply != "000"){ //success popup
          btn_anim__remove("#deposit_dep_2p");
          $(".depReviwBoxDepositBtn a").removeClass("depBtnHide");
          adf_popupError(data.ReplyDesc); //show error
        }
    });
  }

  function adf_modal_terms_popup_error_hide() {
    $(".adf_modal_terms_popup_error p").hide();
  }

  //cancel with terms and conditions click
  $(document).on("click", ".adf_terms_popup_cancel", function() {
    $("#adf_submit_dep_btn")
      .removeAttr("disabled")
      .removeAttr("style");
  });

  //disabled btn send deposit
  function adf_dep_btn_disabled() {
    $("#adf_submit_dep_btn").prop("disabled", true);
    $("#adf_submit_dep_btn").attr("style","background: #689ef9; border: 2px solid #689ef9;");
  }

  //popup edit deposit button click start over
  $(document).on("click", ".depDepErrorPopupEdit", function(e) {
    e.preventDefault();
    $(".depCreditTypesBox").show();
    $("#"+adf_selected_form).show();
    if ($(".depReviwBoxDepositBtn a").hasClass("depNotActiveBtn")) {
      $(".depReviwBoxDepositBtn a").removeClass("depNotActiveBtn");
    }
    $( '#form_standart' ).hide()
  });

  //abled btn send deposit
  function adf_dep_btn_abled() {
    $("#adf_submit_dep_btn").prop("disabled", false);
    $("#adf_submit_dep_btn").removeAttr("style");
  }

  //remove error on focus
  $(document).on("focus", ".depFieldFocus", function() {
    $(this).removeClass("adf_error");
    $(this).parent().parent().find('.lab').removeClass('field__label');
  });

  $(document).on("input", "#CardNum", function() {
    adf_detectCreditCard($(this).val());
    this.value = adf_cc_format(this.value);
  });

  //Currency disabled style
  $(function() {
    if ($("#Currency").is(":disabled")) {
      $("#Currency").addClass("currencySelectDisabled");
    }
  });

  /*wide review*/
  function adf_ReviewWideShow() {
    $(".thisrightacc").addClass("depReviewBoxWide");
    $(".dep--md-6").addClass("depReviewBoxCenter");
    $(".dep--container").attr(
      "style",
      "width: 100%; padding: 0; overflow: hidden;"
    );
  }
  /*submit deposit animation*/
  function submit_dep_animation() {
    $("#adf_submit_dep_btn").addClass("adf_onclic", 250);
  }

  function adf_dep_btn_validate(elem) {
    setTimeout(function() {
      $(elem).removeClass("adf_onclic");
      $(elem).addClass("adf_validate", 450, adf_dep_btn_callback(elem));
    }, 2250);
  }

  function adf_dep_btn_callback(elem) {
    setTimeout(function() {
      $(elem).removeClass("adf_validate");
    }, 1250);
  }

  function adf_sendDepErrorAnimationEnd() {
    $("#adf_submit_dep_btn").removeClass("adf_onclic");
  }

  function adf_detectCreditCard(number) {
    var found_a_card = false;
    if (!found_a_card) {
      var re = new RegExp("^4");
      if (number.match(re) != null) {
        $(".credit_card_icon").addClass("visa_icon");
        $(".credit_card_icon").removeClass("mastercard_icon");
        $(".credit_card_icon").removeClass("JSB_icon");
        found_a_card = true;
      }
    }
    if (!found_a_card) {
      var re = new RegExp("^5[1-5]");
      if (number.match(re) != null) {
        $(".credit_card_icon").addClass("mastercard_icon");
        $(".credit_card_icon").removeClass("visa_icon");
        $(".credit_card_icon").removeClass("JSB_icon");
        found_a_card = true;
      }
    }
    if (!found_a_card) {
      var re = new RegExp("^35(2[89]|[3-8][0-9])");
      if (number.match(re) != null) {
        $(".credit_card_icon").addClass("JSB_icon");
        $(".credit_card_icon").removeClass("visa_icon");
        $(".credit_card_icon").removeClass("mastercard_icon");
        found_a_card = true;
      }
    }
    if (!found_a_card) {
      $(".credit_card_icon").removeClass("JSB_icon");
      $(".credit_card_icon").removeClass("visa_icon");
      $(".credit_card_icon").removeClass("mastercard_icon");
      return false;
    } else {
      return true;
    }
  }

  function adf_cc_format(value) {
    // format card number
    var v = value.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
    var matches = v.match(/\d{4,16}/g);
    var match = (matches && matches[0]) || "";
    var parts = [];
    for (var i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }
    if (parts.length) {
      return parts.join(" ");
    } else {
      return value;
    }
  }

  // CVV HELP CLICK
  function adf_infoBox(elem) {
    var elem = elem;
    var infoType = elem.data("info-name");
    var thisContent = elem.closest(".dep_inf_bx").find(".dep_inf_bx_win");
    thisContent.toggle();
  }

  if (
    $(window).width() < 768 ||
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/) ||
    navigator.userAgent.match(/Windows Phone/i) ||
    navigator.userAgent.match(/ZuneWP7/i)
  ) {
    $(".dep_inf_bx_btn").click(function() {
      adf_infoBox($(this));
    });
  } else {
    $(".dep_inf_bx_btn").hover(function() {
      adf_infoBox($(this));
    });
  }
  
  
  function print_r(arr, level) {
    var print_red_text = "";
    if(!level) level = 0;
    var level_padding = "";
    for(var j=0; j<level+1; j++) level_padding += "    ";
    if(typeof(arr) == 'object') {
      for(var item in arr) {
        var value = arr[item];
        if(typeof(value) == 'object') {
          print_red_text += level_padding + "'" + item + "' :\n";
          print_red_text += print_r(value,level+1);
        }else {
          print_red_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
        }
      }
    }else{
      print_red_text = "===>"+arr+"<===("+typeof(arr)+")";
    }
    return print_red_text;
  }  
  
//end info box
  
})(jQuery);