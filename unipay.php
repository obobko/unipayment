<?php 
/*
Plugin Name: unipay
Version: 1.0
Plugin URI: #
Description: unipay form
Author: OB
Author URI: #
Text Domain: unipay
*/

if ( ! defined( 'ABSPATH' ) ){
	exit;
}

if (session_status() == PHP_SESSION_NONE) {
session_start();
}

define( 'DP_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once DP_PLUGIN_PATH.'includes/curconv.php';

class unipay {

	var $user_info = array(    
		'login' => '',    
		'email' => '',    
		'phone' => '',    
		'address' => '',    
		'country' => '',    
		'city' => '',    
		'zip_code' => '',    
		'firstDepositDate' => '',    
		'balance' => '',    
		'currency' => '',    
		'rememberBilling' => ''  
	); 

	var $adf_form_default = '';
	var $trans_type  = 0;
	var $depmin;

	function __construct(){
		if(!isset($_SESSION['leadid']))
			$leadid = 0;
		else
			$leadid = $_SESSION['leadid'];
		
		$this->depmin = get_option('depmin');
	
		if(!$this->depmin || empty($this->depmin))
			$this->depmin = 250;
			
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, CRM_URL.'ajax/psp/getpsp.php');      
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);      
					curl_setopt($curl, CURLOPT_POST, true);      
					curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query(array('leadid' => $leadid))));      
					$out = curl_exec($curl);      
					curl_close($curl); 
					
		if(isset($_GET['adf']))
			$this->adf_form_default = $_GET['adf'];
		else
			$this->adf_form_default = $out;
		
		if($this->adf_form_default == 'nextpay pre-authorisation')
		{
			$this->trans_type = 1;
		}

		add_action('wp_enqueue_scripts', array(&$this, 'manage_scripts'), 200); //add js, css  
		add_shortcode('addDeposiForm', array(&$this,'unipayForm'));        
		
		add_action( 'wp_ajax_paymentBridger', array(&$this, 'paymentBridger') );
		add_action('wp_ajax_nopriv_paymentBridger', array(&$this, 'paymentBridger'));			
	}   

	 function manage_scripts(){
		wp_register_script('unipay_script', plugins_url() . '/unipayForm/assets/js/validationData.js', '', '', true);    
		wp_register_style('third_page_style', plugins_url() . '/unipayForm/assets/font-awesome-4.7.0/css/font-awesome.min.css');    
		wp_register_style('unipay_style', plugins_url() . '/unipayForm/assets/css/style.css');        
		wp_enqueue_script( 'unipay_script' );    
		wp_enqueue_style( 'third_page_style' );   
		wp_enqueue_style( 'unipay_style' );  
	 }

	/*  * add Deposit Form  *  */

	function unipayForm(){
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		/*****************************************************Test session var***************************************************************************/
		// $_SESSION['login'] = '183365'; // for localhost

		if(!isset($_SESSION['login']) && empty($_SESSION['login'])) {      
			return 'Please log in to see the deposit page';    
		}    

		$user_info = array(
			'login' => $_SESSION['login'],      
			'email' => $_SESSION['email'],      
			'phone' => $_SESSION['phone'],      
			'address' => $_SESSION['lane'],      
			'country' =>$_SESSION['country'],      
			'city' => $_SESSION['city'],      
			'zip_code' => $_SESSION['code'],      
			'firstDepositDate' => $_SESSION['firstDepositDate'],      
			'balance' => $_SESSION['balance'],      
			'currency' => $_SESSION['currency'],      
			'rememberBilling' => $_SESSION['rememberBilling'] 
		);

		include( plugin_dir_path( __FILE__ ) . 'includes/countries.php');    
		$file = plugin_dir_path( __FILE__ ) . 'includes/forms/form.php';        
		include( $file );  
	}
	
	function getUserIp(){
		$client = @$_SERVER['HTTP_CLIENT_IP'];    
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];    
		$remote = @$_SERVER['REMOTE_ADDR'];   
  
		if (filter_var($client, FILTER_VALIDATE_IP)) {      
			$ip = $client;    
		} elseif (filter_var($forward, FILTER_VALIDATE_IP)) {     
			$ip = $forward;    
		} else {      
			$ip = $remote;    
		} 
		return $ip;
	}

	function getLocationInfoByIp(){
		$client = @$_SERVER['HTTP_CLIENT_IP'];    
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];    
		$remote = @$_SERVER['REMOTE_ADDR'];   
		$result = array('country' => '', 'city' => '');    
		if (filter_var($client, FILTER_VALIDATE_IP)) {      
			$ip = $client;    
		} elseif (filter_var($forward, FILTER_VALIDATE_IP)) {     
			$ip = $forward;    
		} else {      
			$ip = $remote;    
		} 
		$ip_data = json_decode($this->curl_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));    
		if ($ip_data && $ip_data->geoplugin_countryName != null) {      
			$result['country'] = $ip_data->geoplugin_countryName;
			$result['countryCode'] = $ip_data->geoplugin_countryCode;      
			$result['city'] = $ip_data->geoplugin_city;      
			$result['ip'] = $ip;    
		}    
		return $result;     
	}   

	/*  * get location by IP part 2   *  */  

	function curl_get_contents($url){        
		$ch = curl_init();    
		curl_setopt($ch, CURLOPT_HEADER, 0);    
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
		curl_setopt($ch, CURLOPT_URL, $url);   
		$data = curl_exec($ch);    
		curl_close($ch);    
		return $data;     
	}   
	/*  * add Text Domain  *  */   

	function loadTextDomain(){        
	  load_plugin_textdomain( 'unipayForm', false, dirname( plugin_basename(__FILE__) ) .'/includes/lang/' );     
	}  
	

function bridge_authorisation(){

  $data = array(
    "user_name" => "m", 
    "password" => "",
  );
  
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.bridgerpay.com/v1/auth/login",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => urldecode(json_encode($data)),
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  
  $response = json_decode($response, true);
  
  if($response['response']['code'] != 200)
	throw new Exception('Authentification failed. '.$response['response']['code'].' '.$response['response']['message']);
		
  return $response['result']['access_token']['token'];
}

function paymentBridger()
{
	$curconv = false;
		
	if(!isset($_SESSION['login']))
	{
		echo json_encode(array('result' => false, 'message' => 'You are not logged'));
		wp_die();
	}

	if(!empty($_POST))
		$data  = $_POST;
	elseif(!empty($_GET))
		$data = $_GET;
	else
	{
		echo json_encode(array('result' => false, 'message' => 'No data provided'));
		wp_die();
	}
	
	$data['currency'] = $data['Currency'];
	$data['amount'] = $data['Amount'];
	
	if($data['currency'] == 2 || $data['currency'] == 'EUR')
		$data['currency'] = 'EUR';
	elseif($data['currency'] == 1 || $data['currency'] == 'USD')
		$data['currency'] = 'USD';
	elseif($data['currency'] == 3 || $data['currency'] == 'GBP')
		$data['currency'] = 'GBP';
	
	if(isset($_SESSION['bridge_token']) && isset($_SESSION['bridge_exp']) && (($_SESSION['bridge_exp']+7200) < date('U')))
	{
		$bridge_token = $_SESSION['bridge_token'];
	}
	else
	{
		try
		{
			$bridge_token = $this->bridge_authorisation();
		}
		catch(Exception $e) 
		{
			echo json_encode(array('result' => false, 'message' => $e->getMessage()));
			wp_die();
		}
		$_SESSION['bridge_exp'] = date('U');
		$_SESSION['bridge_token'] = $bridge_token;
	}
	
	if($data['currency'] != 'EUR' && $curconv)
	{
		require_once DP_PLUGIN_PATH.'includes/curconv2.php';
		$rates = getRate2();
		
		if($data['currency'] == 'GBP')
		{		
			$amount = $data['amount'] / $rates['eur2gbp'];
			$currency = 'EUR';
		}
		elseif($data['currency'] == 'USD')
		{			
			$amount = $data['amount'] / $rates['eur2usd'];
			$currency = 'EUR';
		}
		
		$amount = round($amount, 2);
	}
	else
	{
		$amount = $data['amount'];
		$currency =  $data['currency'];
	}
	
	$orderid = "order_".time().mt_rand(0,99);
	
	$input = array(
    "user_name" => "", 
    "password" => "",
    "cashier_key"=>"",

    "order_id" => $orderid,

    "currency"=>$currency,
    "country" => $data['BillingCountry'],
    "amount"=> $amount,

    "amount_lock" => "true",
    "currency_lock" => "true",

    "address"=>$data['BillingAddress1'],
    "state"=>"",
    "city"=>$data['BillingCity'],
    "zip_code"=>$data['BillingZipCode'],

    "phone"=>$_SESSION['phone'],
    "email"=>$_SESSION['email'],

  );

 $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.bridgerpay.com/v1/cashier/session/create/KEY",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => urldecode(json_encode($input)),
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      'Authorization: Bearer '.$bridge_token
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  
  
  $response = json_decode($response, true);
  
  if(isset($response['result']['cashier_token']))
	  $cashier_token = $response['result']['cashier_token'];
  else
	  $cashier_token = '';
  
  $error = '';
  
  if(isset($response['response']['code']) && $response['response']['code'] != '200')
  {
	  foreach($response['result'] as $er)
		$error .= $er['message'].' ';
  }

  
  if(isset($response['response']['status']))
  {
		if($response['response']['code'] == '200')
			echo json_encode(array('result' => true, 'message' => 'SUCCESS', 'url' => 'https://cashier.bridgerpay.com?cashierKey=KEY&cashierToken='.$response['result']['cashier_token'], 'token' => $response['result']['cashier_token']));
		else
			echo json_encode(array('result' => false, 'message' => 'ERROR', 'response' => $response));
  }
  else
	echo json_encode(array('result' => false, 'message' => 'Unknown error'));

	wp_die();
}


}

$a = new unipay();

?>